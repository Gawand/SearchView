package com.example.admin.searchview;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Locale;
import java.util.zip.Inflater;


public class MainActivity extends AppCompatActivity {
    ArrayList arrayList;
    RecyclerView recyclerView;
    Toolbar toolbar;
    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        arrayList=new ArrayList();
        recyclerView= (RecyclerView) findViewById(R.id.rv);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        arrayList.add("avinash");
        arrayList.add("sachin");
        arrayList.add("hitesh");
        arrayList.add("bipin");
        arrayList.add("sunil");

        listAdapter = new ListAdapter(MainActivity.this,arrayList);

        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.
                menus, menu);
        Button button=new Button(MainActivity.this);
        button.setBackground(getResources().getDrawable(R.drawable.ic_done_white_24dp));
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(toolbar.getHeight() * 2 / 3, toolbar.getHeight() * 2 / 3);
        layoutParams.setMargins(8,8,8,8);
        button.setLayoutParams(layoutParams);


        //final MenuItem item = menu.findItem(R.id.search_icon);
       // final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //searchView.setOnQueryTextListener(this);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
         final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search_icon));

        ((LinearLayout)searchView.getChildAt(0)).addView(button);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setIconifiedByDefault(true);
     //   MenuItemCompat menuItem=(MenuItemCompat.findItem(R.id.search_icon);



        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                listAdapter.getFilter().filter(searchView.getQuery());
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public String TAG="SearchView";

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: ");
                listAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange: ");

                return true;
            }
        });
        
        return true;
    }

}
